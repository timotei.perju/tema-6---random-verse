$(document).ready(function () {

  $("#get_quote").html("Get Verse");

  $("#bible").click(function () {
    $(this).addClass("act");
    $("#celeb").removeClass("act");
    $("#get_quote").html("Get Verse");
  });

  var getBible = function () {
    $.ajax({
      url: "https://labs.bible.org/api/?passage=random&type=json&callback=myCallback",
      crossDomain: true,
      dataType: 'jsonp',
      success: function (result) {
        var data = [];
        data.quote = result[0].text;
        data.author = result[0].bookname + " " + result[0].chapter + ":" + result[0].verse;
        renderer(data);
      }
    });
  }

  var renderer = function (arr) {
    $("#quote-title").html(arr.author);
    $("#quote-content").html(arr.quote);
    $("#quote-content").children().remove();
  }
  getBible();

});