//get bible API
function generate() {
    var book = ["Genesis", "Exodus", "Leviticus", "Numbers", "Deuteronomy", "Joshua", "Judges", "Ruth", "1 Samuel", "2 Samuel",
        "1 Kings", "2 Kings", "1 Chronicles", "2 Chronicles", "Ezra", "Nehemiah", "Esther", "Job", "Psalms", "Proverbs", "Ecclesiastes",
        "Song of Solomon", "Isaiah", "Jeremiah", "Lamentations", "Ezekiel", "Daniel", "Hosea", "Joel", "Amos", "Obadiah", "Jonah",
        "Micah", "Nahum", "Habakkuk", "Zephaniah", "Haggai", "Zechariah", "Malachi", "Matthew", "Mark", "Luke", "John", "Acts",
        "Romans", "1 Corinthians", "2 Corinthians", "Galatians", "Ephesians", "Philippians", "Colossians", "1 Thessalonians",
        "2 Thessalonians", "1 Timothy", "2 Timothy", "Titus", "Philemon", "Hebrews", "James", "1 Peter", "2 Peter", "1 John",
        "2 John", "3 John", "Jude", "Revelation"
    ];

    var chapter_nr = Math.round((Math.random() * book.length));
    console.log("chapter_nr:" + chapter_nr);

    console.log(book[chapter_nr]);

    var verse_nr = Math.floor(Math.random() * 1 + 1);
    console.log("verse_nr: "+verse_nr);

    var verse = Math.floor(Math.random() * 1 + 1);
    console.log("verse: " + verse);

    jQuery.ajax({
        url: 'https://getbible.net/json?',
        dataType: 'jsonp',
        data: 'p=' + book[chapter_nr] + verse_nr + ':' + verse + '&v=kjv',
        jsonp: 'getbible',
        success: function (json) {
            // set text direction
            console.log(json);
            if (json.direction == 'RTL') {
                var direction = 'rtl';
            } else {
                var direction = 'ltr';
            }
            // check response type
            if (json.type == 'verse') {
                var output = '';
                jQuery.each(json.book, function (index, value) {
                    output += '<br> <center><b>' + value.book_name + ' ' + value.chapter_nr + ':</b></center><p class="' + direction + '">';
                    jQuery.each(value.chapter, function (index, value) {
                        output += '<small class="ltr">' + value.verse_nr + '</small>';
                        output += value.verse;
                        output += '<br>';
                    });
                    output += '</p>';
                });
                jQuery('#text').html(output); // <---- this is the div id we update
            }
        },
        error: function () {
            jQuery('#text').html('<br><h4>No scripture was returned, please try again!</h4>'); // <---- this is the div id we update
        },
    });
}

generate();